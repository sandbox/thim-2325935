<?php

/**
 * Master form which calls an individual form for each step.
 *
 * @see translation_export_form_validate().
 * @see translation_export_form_submit().
 *
 * @param type $form
 * @param string $form_state
 * @return type
 */
function entity_translation_export_tax_form($form, &$form_state) {

  if (!isset($form_state['stage'])) {
    $form_state['stage'] = 'select_taxonomy';
  }

  $form = array();
  switch ($form_state['stage']) {

    case 'select_taxonomy':
      return entity_translation_export_tax_taxonomy_form($form, $form_state);
      break;
    case 'select_language':
      return entity_translation_export_tax_language_form($form, $form_state);
      break;
    case 'export_options':
      return entity_translation_export_tax_options_form($form, $form_state);
      break;

    default:
      return entity_translation_export_tax_language_form($form, $form_state);
      break;
  }

  return $form;
}


/**
 * Form for the select_language step.
 *
 * @see entity_translation_export_tax_form().
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_translation_export_tax_language_form($form, &$form_state) {

  $values = isset($form_state['multistep_values']['select_language']) ? $form_state['multistep_values']['select_language'] : array();

  $languages = locale_language_list();

  $form['select_language']['available_languages'] = array(
    '#type' => 'radios',
    '#title' => 'Please select your translation language?',
    '#options' => $languages,
    '#default_value' => isset($values['available_languages']) ? $values['available_languages'] : NULL,
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back')
  );

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next')
  );

  return $form;
}


/**
 * Form for the select_taxonomy step.
 *
 * @see entity_translation_export_tax_form().
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_translation_export_tax_taxonomy_form($form, &$form_state) {

  $values = isset($form_state['multistep_values']['select_taxonomy']) ? $form_state['multistep_values']['select_taxonomy'] : array();

  // Get taxonomy vocabularies
  $ctypes = taxonomy_get_vocabularies();

  foreach ($ctypes as $ctype) {
    // Get fields of content type
    $fields = field_info_instances("taxonomy_term", $ctype->machine_name);

    $labels = '';
    foreach ($fields as $field) {
      if (isset($field['widget']) && substr($field['widget']['type'], 0, 5) == 'text_') {
        $labels[$field['widget']['weight']] .= $field['label'];
      }
    }
    ksort($labels);

    $label_txt = '';
    foreach ($labels as $label) {
      $label_txt .= $label . ', ';
    }
    $label = substr($label_txt, 0, -2);

    // Create radios array
    if (!empty($labels)) {
      $txt_fields[$ctype->machine_name] = ucfirst($ctype->machine_name) . ' ( ' . $label . ' )';
    }
  }

  $form['select_taxonomy']['taxonomy'] = array(
    '#type' => 'radios',
    '#title' => 'Please select the content type you wish to export?',
    '#options' => $txt_fields,
    '#default_value' => isset($values['taxonomy']) ? $values['taxonomy'] : NULL,
  );

  $form['select_taxonomy']['status'] = array(
    '#type' => 'radios',
    '#title' => 'Please select the content status?',
    '#options' => array(1 => 'Published only', 0 => 'Unpublished only', 'ALL' => 'Published and Unpublished'),
    '#default_value' => isset($values['status']) ? $values['status'] : 1,
  );

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next')
  );

  return $form;
}


/**
 * Form for the export_options step.
 *
 * @see entity_translation_export_tax_form().
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_translation_export_tax_options_form($form, &$form_state) {

  $values = isset($form_state['multistep_values']['export_options']) ? $form_state['multistep_values']['export_options'] : array();

  $form['export_options']['export_format'] = array(
    '#type' => 'radios',
    '#title' => 'Please select your prefered export document format?',
    '#options' => array(
      'Word2007' => 'MS Word (.docx)',
      'ODText' => 'OpenDocument (.odf)',
      // 'RTF' => 'Rich Text Format (.rtf)', //Breaks because of images in the content
      // 'HTML' => 'HTML (.html)'
    ),
    '#default_value' => isset($values['export_format']) ? $values['export_format'] : 'Word2007',
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back')
  );

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Export')
  );

  return $form;
}

/**
 * Master validation function for the multi-step form - uses per-stage
 * validation and calls functions for each one.
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_translation_export_tax_form_validate($form, &$form_state) {

  if ($form_state['triggering_element']['#value'] == 'Back') {
    return;
  }

  switch ($form_state['stage']) {
    case 'select_language':
      return entity_translation_export_tax_language_validate($form, $form_state);
      break;
    case 'select_taxonomy':
      return entity_translation_export_tax_taxonomy_validate($form, $form_state);
      break;
    case 'export_options':
      return entity_translation_export_tax_options_validate($form, $form_state);
      break;
  }
}

/**
 * Validation for the select_language step
 *
 * @param type $form
 * @param type $form_state
 */
function entity_translation_export_tax_language_validate($form, &$form_state) {
  if (!$form_state['values']['available_languages']) {
    form_set_error('available_languages', 'You must select a language before continuing');
  }
}

/**
 * Validation for the select_taxonomy step
 *
 * @param type $form
 * @param type $form_state
 */
function entity_translation_export_tax_taxonomy_validate($form, &$form_state) {
  if (!$form_state['values']['taxonomy']) {
    form_set_error('taxonomy', 'You must select a content type before continuing');
  }
}

/**
 * Validation for the export_options step
 *
 * @param type $form
 * @param type $form_state
 */
function entity_translation_export_tax_options_validate($form, &$form_state) {
  if (!$form_state['values']['export_format']) {
    form_set_error('export_format', 'You select a export format.');
  }
}

/**
 * Given the current stage the user is on, calculate what the next step would be
 *
 * @param type $form_state
 * @return string
 */
function entity_translation_export_tax_move_to_next_stage($form, &$form_state) {
  switch ($form_state['stage']) {
    case 'select_taxonomy':
      return 'select_language';
      break;

    case 'select_language':
      return 'export_options';
      break;
  }
}

/**
 * Given the current stage the user is on, calculate what the previous step
 * would be
 *
 * @param type $form_state
 * @return string
 */

function entity_translation_export_tax_move_to_previous_stage($form, &$form_state) {
  switch ($form_state['stage']) {
    case 'select_language':
      return 'select_taxonomy';
      break;

    case 'export_options':
      return 'select_language';
      break;
  }
}

/**
 * Handles what to do with the submitted form depending on what stage has been
 * completed.
 *
 * @see translation_export_form()
 * @see translation_export_form_validate()
 *
 * @param type $form
 * @param type $form_state
 */
function entity_translation_export_tax_form_submit($form, &$form_state) {

  switch ($form_state['stage']) {

    case 'export_options':
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      if ($form_state['triggering_element']['#value'] != 'Back') {
        entity_translation_export_tax_options_submit($form, $form_state);
        $form_state['complete'] = TRUE;
      }
      break;

    default:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      $form_state['new_stage'] = entity_translation_export_tax_move_to_next_stage($form, $form_state);
      break;

  }

  //  if (isset($form_state['complete'])) drupal_goto('complete-page');

  if ($form_state['triggering_element']['#value'] == 'Back') {
    $form_state['new_stage'] = entity_translation_export_tax_move_to_previous_stage($form, $form_state);
  }

  if (isset($form_state['multistep_values']['form_build_id'])) {
    $form_state['values']['form_build_id'] = $form_state['multistep_values']['form_build_id'];
  }
  $form_state['multistep_values']['form_build_id'] = $form_state['values']['form_build_id'];
  $form_state['stage'] = $form_state['new_stage'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Handles the submission of the final stage
 *
 * Generate a document and prepare it for export.
 *
 * @param type $form
 * @param type $form_state
 */
function entity_translation_export_tax_options_submit($form, &$form_state) {
  // Form values
  $multstep_values = $form_state['multistep_values'];

  $vocabulary = $multstep_values['select_taxonomy']['taxonomy'];
  $translation_language = $multstep_values['select_language']['available_languages'];
  $format = $multstep_values['export_options']['export_format'];

  // Available export writers
  $writers = array('Word2007' => '.docx', 'ODText' => '.odt', 'RTF' => '.rtf', 'HTML' => '.html', 'PDF' => '.pdf');

  // Get export translation data
  $data = entity_translation_export_get_taxonomy_translations_data($vocabulary, $translation_language);

  // Set filename
  $filename = $vocabulary . '_' . $translation_language . $writers[$format];

  // Generate file for export
  $file_url = entity_translation_export_generate_document($data, $filename, $format);

  // Generate file for export using batch
  //  $directory_path = 'public://entity_translation_export/';
  //  file_unmanaged_delete($directory_path.$filename);
  //  entity_translation_export_process_batch($data, $filename, $format);

  // Set message with download link
  drupal_set_message('You can download the document here: ' . l($filename, $file_url));
}
